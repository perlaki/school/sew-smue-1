const express = require('express');

const employee = require('../models/employee')

const router = express.Router();

router.get('/employee', function(req, res, next) {
  res.json(employee.all());
});

router.post('/employee', function(req, res, next) {
    console.log(req.body)
    res.json(employee.add(req.body));
});
  
router.put('/employee', function(req, res, next) {
    res.json(employee.toggle(req.body));
});
  
module.exports = router;
