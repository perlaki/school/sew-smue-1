const express = require('express');
const {department} = require('../models/employee')

const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {departments: department()});
});

module.exports = router;
