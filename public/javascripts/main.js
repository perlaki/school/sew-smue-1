const table = document.getElementById('table')


function toggle(event) {
    let elem = event.target.parentElement.parentElement
    let data = JSON.parse(elem.dataset.json)
    let dep = elem.dataset.department
    let b = JSON.stringify({
        employee: data,
        department: dep
    })
    console.log(b)
    fetch('/api/employee', {
        method: 'PUT',
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
        body: b
    }).then(res => res.json()).then(data => {
        location.href = "/"
    })
}


fetch('/api/employee').then(res => res.json()).then(data => {
    for (let d of Object.keys(data)) {
        for (let e of data[d]) {
            let temp = document.createElement('template')
            temp.innerHTML = `<tr data-json='${JSON.stringify(e)}' data-department="${d}"><td><input type="checkbox" ${e.active ? 'checked' :''} class="active"></td><td>${d}</td><td>${e.firstName} ${e.lastName}</td></tr>`
            table.appendChild(temp.content)
        }
    }

    document.querySelectorAll('.active').forEach(element => {
        element.onchange = toggle
    })
})


document.getElementById('add').addEventListener('submit', async function (event) {
    event.preventDefault()

    await fetch(this.action, {
        method: this.method,
        body: JSON.stringify(formToJson(this)),
        headers: {
            "Content-Type": "application/json; charset=utf-8"
        },
    })
})

function formToJson(element) {
    let form = new FormData(element)
    let data = {}
    for (let k of form.keys()) {
        data[k] = form.get(k)
    }
    return data
}