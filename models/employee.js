const db = require('../emp.json')

class employee {
    constructor({firstName, lastName, birthDate, department}) {
        this.firstName = firstName
        this.lastName = lastName
        this.birthDate = birthDate
        this.active = true
        db[department].push(this)
    }
}

function department(){
    return Object.keys(db)
}

function all() {
    return db
}

function get(emp, dep) {
    return db[dep].find(e => e.firstName == emp.firstName && e.lastName == emp.lastName)
}

function add(emp) {
    return new employee(emp)
}

function toggle({employee, department}) {
    let e = get(employee, department)
    e.active = !e.active
    return e
}


module.exports = { all, add, department, toggle }